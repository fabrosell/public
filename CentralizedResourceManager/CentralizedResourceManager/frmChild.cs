﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CentralizedResourceManager
{
    public partial class frmChild : Form, I2DMotoroleable
    {
        public frmChild()
        {
            InitializeComponent();
        }

        public void setTitle(String title)
        {
            this.Text = title;
        }

        public void CodigoPistoleado(String codigo)
        {
            rtbCode.Text = codigo;

            Debug.WriteLine(String.Format("Call to delegate on \"{0}\" with code \"{1}\"",this.Text, codigo));
        }

        public void LimpiarCodigo()
        {
            rtbCode.Text = String.Empty;
        }
    }
}
