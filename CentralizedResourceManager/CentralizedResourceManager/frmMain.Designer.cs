﻿namespace CentralizedResourceManager
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.msMenu = new System.Windows.Forms.MenuStrip();
            this.window1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.window2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.window3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.window4ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.window5ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unnafectedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.msMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // msMenu
            // 
            this.msMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.window1ToolStripMenuItem,
            this.window2ToolStripMenuItem,
            this.window3ToolStripMenuItem,
            this.window4ToolStripMenuItem,
            this.window5ToolStripMenuItem,
            this.unnafectedToolStripMenuItem});
            this.msMenu.Location = new System.Drawing.Point(0, 0);
            this.msMenu.Name = "msMenu";
            this.msMenu.Size = new System.Drawing.Size(1015, 24);
            this.msMenu.TabIndex = 1;
            this.msMenu.Text = "menuStrip1";
            // 
            // window1ToolStripMenuItem
            // 
            this.window1ToolStripMenuItem.Name = "window1ToolStripMenuItem";
            this.window1ToolStripMenuItem.Size = new System.Drawing.Size(72, 20);
            this.window1ToolStripMenuItem.Text = "Window 1";
            this.window1ToolStripMenuItem.Click += new System.EventHandler(this.window1ToolStripMenuItem_Click);
            // 
            // window2ToolStripMenuItem
            // 
            this.window2ToolStripMenuItem.Name = "window2ToolStripMenuItem";
            this.window2ToolStripMenuItem.Size = new System.Drawing.Size(72, 20);
            this.window2ToolStripMenuItem.Text = "Window 2";
            this.window2ToolStripMenuItem.Click += new System.EventHandler(this.window2ToolStripMenuItem_Click);
            // 
            // window3ToolStripMenuItem
            // 
            this.window3ToolStripMenuItem.Name = "window3ToolStripMenuItem";
            this.window3ToolStripMenuItem.Size = new System.Drawing.Size(72, 20);
            this.window3ToolStripMenuItem.Text = "Window 3";
            this.window3ToolStripMenuItem.Click += new System.EventHandler(this.window3ToolStripMenuItem_Click);
            // 
            // window4ToolStripMenuItem
            // 
            this.window4ToolStripMenuItem.Name = "window4ToolStripMenuItem";
            this.window4ToolStripMenuItem.Size = new System.Drawing.Size(72, 20);
            this.window4ToolStripMenuItem.Text = "Window 4";
            this.window4ToolStripMenuItem.Click += new System.EventHandler(this.window4ToolStripMenuItem_Click);
            // 
            // window5ToolStripMenuItem
            // 
            this.window5ToolStripMenuItem.Name = "window5ToolStripMenuItem";
            this.window5ToolStripMenuItem.Size = new System.Drawing.Size(72, 20);
            this.window5ToolStripMenuItem.Text = "Window 5";
            this.window5ToolStripMenuItem.Click += new System.EventHandler(this.window5ToolStripMenuItem_Click);
            // 
            // unnafectedToolStripMenuItem
            // 
            this.unnafectedToolStripMenuItem.Name = "unnafectedToolStripMenuItem";
            this.unnafectedToolStripMenuItem.Size = new System.Drawing.Size(80, 20);
            this.unnafectedToolStripMenuItem.Text = "Unnafected";
            this.unnafectedToolStripMenuItem.Click += new System.EventHandler(this.unnafectedToolStripMenuItem_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1015, 665);
            this.Controls.Add(this.msMenu);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.msMenu;
            this.Name = "frmMain";
            this.Text = "Main Windows";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.msMenu.ResumeLayout(false);
            this.msMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip msMenu;
        private System.Windows.Forms.ToolStripMenuItem window1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem window2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem window3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem window4ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem window5ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unnafectedToolStripMenuItem;
    }
}

