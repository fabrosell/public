﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using MotorolaDs4208.Api;

namespace CentralizedResourceManager
{
    public partial class frmMain : Form
    {
        private MotorolaScannerApi _motoScannerApi;        


        public frmMain()
        {
            InitializeComponent();

            InitializeMotorolaScanner();
        }

        private void InitializeMotorolaScanner()
        {
            try
            {
                _motoScannerApi = new MotorolaScannerApi(null);
                if (_motoScannerApi != null)
                {
                    _motoScannerApi.Connect();
                    var listScanners = _motoScannerApi.GetScanners();
                    if (listScanners != null && listScanners.Scanners.Count > 0)
                    {
                        var scanner = listScanners.Scanners[0];
                        scanner.ChangeMode(ScannerMode.UsbSnapiWithImage);
                        _motoScannerApi.OnBarcodeEvent += ProcesarPistoleo2D;
                    }
                    else
                    {
                        _motoScannerApi = null;
                    }
                }
            }
            catch (Exception ex)
            {
                _motoScannerApi = null;
            }
        }

        private void showChild(String text)
        {
            frmChild child = new frmChild();
            child.MdiParent = this;
            child.setTitle(text);            
            child.Show();
        }

        private void ProcesarPistoleoInvoked(ScanData scanData)
        {
            try
            {
                Debug.WriteLine(String.Format("Processed: {0}", scanData.Code));

                if (this.MdiChildren != null && this.MdiChildren.Length > 0)
                {
                    foreach (var child in this.MdiChildren)
                    {
                        I2DMotoroleable realChild = child as I2DMotoroleable;

                        if (realChild != null)
                            realChild.LimpiarCodigo();
                    }

                    I2DMotoroleable activeChild = this.ActiveMdiChild as I2DMotoroleable;

                    if (activeChild != null)
                        activeChild.CodigoPistoleado(scanData.Code);
                    
                }
                else
                {
                    Debug.WriteLine("No child open yet...");
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(String.Format("Error: {0}", ex.Message));
            }

        }

        private void window1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showChild(window1ToolStripMenuItem.Text);
        }

        private void window2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showChild(window2ToolStripMenuItem.Text);
        }

        private void window3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showChild(window3ToolStripMenuItem.Text);
        }

        private void window4ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showChild(window4ToolStripMenuItem.Text);
        }

        private void window5ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showChild(window5ToolStripMenuItem.Text);
        }

        private void frmMain_Load(object sender, EventArgs e)
        {

        }

        private void ProcesarPistoleo2D(ScanData scanData)
        {
            Invoke((MethodInvoker)(() =>
            {
                ProcesarPistoleoInvoked(scanData);
            }));
        }

        private void unnafectedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmNotI2DMotoroleable unnafected = new frmNotI2DMotoroleable();
            unnafected.MdiParent = this;
            unnafected.Show();
        }
    }
}
